import React, { useState, useEffect } from 'react';
import "./css/home.css";
import PokemonList from './PokemonList';

function Home() {

    return (

        <div className="background" >
            <h1 className="pokedox"> Pokedox </h1>
            <PokemonList />

        </div>
    )
}

export default Home
