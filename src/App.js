import React from 'react';
import Home from './Home';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Pokemon from './Pokemon';
function App() {
  return (
    <Router>
      <div className="home">
        <Switch>
          <Route exact path='/' component={Home} />
          <Route exact path="/pokemon/:pokemonIndex" component={Pokemon} />

        </Switch>


      </div>
    </Router>

  )
}

export default App
