import React, { useState, useEffect } from 'react';
import PokemonCard from './PokemonCard'
import "./css/Pokemonlist.css";

import axios from 'axios';



function Pokemonlist() {
    const [pokemon, setPokemon] = useState([]);
    const [NextUrl, setNextUrl] = useState('');
    const [prevUrl, setPrevUrl] = useState('');
    const [CurrentPageUrl, setCurrentPageUrl] = useState("https://pokeapi.co/api/v2/pokemon/");



    const [loading, setLoading] = useState('');




    useEffect(() => {
        async function fetchData() {

            await axios.get(CurrentPageUrl).then(
                res => {
                    setPokemon(res.data['results']);
                    console.log(res);
                    setNextUrl(res.data.next);
                    setPrevUrl(res.data.previous);
                    setLoading(false);

                }


            )

        }

        fetchData();


    }, [CurrentPageUrl])

    function gotoNextPage() {
        setCurrentPageUrl(NextUrl);

    }
    function gotoPrevPage() {
        setCurrentPageUrl(prevUrl);
    }


    return (
        <div>
            {!loading ? <>(<div >
                {pokemon.map(pokemon =>
                    <PokemonCard key={pokemon.name} pokemon={pokemon.name} url={pokemon.url} />

                )}
            </div>
                {prevUrl ? <button className="Previous" onClick={gotoPrevPage}>Previous</button> : null}
                {NextUrl ? <button className="Next" onClick={gotoNextPage}>Next</button> : null}

            )
            </>

                : (<h1> Pokemon is Loading</h1>)}
        </div>

    )
}



export default Pokemonlist;
