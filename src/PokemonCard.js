import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import Pokemon from './Pokemon';


import "./css/pokemoncard.css";
import classNames from 'classnames';
import ash from './images/ash.gif';

import { Card, CardActionArea, CardMedia, makeStyles, Typography } from '@material-ui/core';


function PokemonCard({ pokemon, url }) {
    const pokemonIndex = url.split('/')[url.split('/').length - 2];
    console.log(pokemonIndex);
    const [imageloading, setImageloading] = useState(true);


    useEffect(() => {
        setImageloading(true);
    }, [])




    const useStyles = makeStyles((theme) => ({
        justifycontent: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',

            fontFamily: 'Pokemon',
            fontWeight: 'bold',
            fontWeight: '1000'
        },
        media: {
            height: '270px',
            paddingBottom: '10px',
            margin: '0 0 13px 0'
        }
    }))
    const classes = useStyles();


    return (
        <Link to={`Pokemon/${pokemonIndex}`}>
            <Card className="pokecard" variant="outlined">
                <CardActionArea href="" target="_blank">
                    <CardMedia className={classes.media} image={!imageloading ? ash : `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${pokemonIndex}.png`} >
                    </CardMedia>
                    <div className="">
                        <Typography variant="h3" color="textSecondary" className={classes.justifycontent}>
                            {pokemon.charAt(0).toUpperCase() + pokemon.slice(1)}

                        </Typography>
                    </div>


                </CardActionArea>
            </Card >
        </Link>


    )
}

export default PokemonCard
