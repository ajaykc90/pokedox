import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useParams } from "react-router";
import "./css/pokemon.css";

function Pokemon() {
    const [name, setName] = useState([]);
    const [baseexperience, setBaseexperience] = useState('');
    let [hp, sethp] = useState('');
    const [shape, setShape] = useState('');
    const [attack, setAttack] = useState('');
    const [defense, setdefense] = useState('');
    const [speed, setspeed] = useState('');
    const [specialAttack, setspecialAttack] = useState('');
    const [specialDefense, setspecialDefense] = useState('');
    const [image, setimage] = useState('');
    const [abilities, setabilities] = useState([]);
    const [height, setheight] = useState('');
    const [weight, setWeight] = useState('');
    const [eggGroup, seteggGroup] = useState('');
    const [genderRatioMale, setGenderRatioMale] = useState('');
    const [genderRatioFemale, setGenderRatioFemale] = useState('');
    const [hatchsteps, sethatchSteps] = useState('');
    const [description, setdescription] = useState('');
    let { pokemonIndex } = useParams();
    const [types, setTypes] = useState([]);
    const [legendary, setLegendary] = useState('');
    const [mythical, setMythical] = useState('');
    const [habitat, sethababitat] = useState('');
    const [color, setColor] = useState('');
    const [catchratio, setcatchratio] = useState('');


    useEffect(() => {

        async function fetchData() {
            const pokemonurl = `https://pokeapi.co/api/v2/pokemon/${pokemonIndex}`;
            const pokemonSpeciesurl = `https://pokeapi.co/api/v2/pokemon-species/${pokemonIndex}`;
            await axios.get(pokemonurl).then(
                res => {
                    setName(res.data.name);
                    console.log(res);
                    setimage(res.data.sprites.front_default);
                    setabilities(res.data['abilities']);
                    setTypes(res.data['types']);
                    setBaseexperience(res.data.base_experience);
                    sethp(res.data.stats[0].base_stat);
                    setAttack(res.data.stats[1].base_stat);
                    setdefense(res.data.stats[2].base_stat);
                    setspecialAttack(res.data.stats[3].base_stat);
                    setspecialDefense(res.data.stats[4].base_stat);
                    setspeed(res.data.stats[5].base_stat);
                    setheight(res.data.height);
                    setWeight(res.data.weight);
                    console.log(res.data.stats[0].base_stat);


                },
                await axios.get(pokemonSpeciesurl).then(
                    res => {
                        seteggGroup(res.data.egg_groups.map((e) => e.name));
                        res.data.flavor_text_entries.some(flavor => {
                            if (flavor.language.name === 'en') {
                                setdescription(flavor.flavor_text)
                            }
                        });
                        setGenderRatioMale(12.5 * res.data['gender_rate']);
                        setGenderRatioFemale(12.5 * (8 - res.data['gender_rate']));
                        setLegendary(res.data.is_legendary ? "true" : "false");
                        console.log(res.data.is_legendary);
                        sethababitat(res.data.habitat.name);
                        setColor(res.data.color.name);
                        setMythical(res.data.is_mythical ? "true" : "false");
                        setcatchratio(res.data.capture_rate);
                        sethatchSteps(res.data.hatch_counter);
                        setShape(res.data.shape.name);
                    }
                )
            )
        }
        fetchData();

    }, [])

    return (
        <div className="container"><h1>
            {name.toString().charAt(0).toUpperCase() + name.slice(1)}
        </h1><div>
                <img src={image} className="image" />
            </div>
            <h2>{description}</h2>
            <h2>
                Types : {types.map((t) =>
                <p key={t.type.name} >
                    💠{t.type.name}
                </p>)}
            </h2>
            <h2 className="row">Abilities name :
            {abilities.map((a) => (
                <p key={a.ability.name} className="abilityname">
                    🌟 {a.ability.name}
                </p>
            ))}
            </h2>
            <h2> Base Experience:
                {baseexperience}
            </h2>
            <h2> Capture Ratio: {catchratio}</h2>
            <h2> Color: {color}</h2>
            <h2> Height : {Math.round((height * 0.3280084 + 0.0001) * 100 / 100)} ft</h2>
            <h2> Weight {Math.round((weight * 0.220462 + 0.0010) * 100 / 100)} lbs</h2>
            <h2> Hatch Counter : {hatchsteps}</h2>
            <h2> Shape: {shape}</h2>
            <h2> Hp: {hp}</h2>
            <h2> Attack: {attack}</h2>
            <h2> Defense: {defense}</h2>
            <h2> Special Attack: {specialAttack}</h2>
            <h2>Special Defense: {specialDefense}</h2>
            <h2>Speed: {speed}</h2>
            <h2 className="row">EggGroup:
            {eggGroup}
            </h2>
            <h2>Gender Ratio of Male: {genderRatioMale}</h2>
            <h2> Gender Ratio of Female : {genderRatioFemale}</h2>
            <h2> Habitat: {habitat}</h2>
            <h2> IS Legendary:  {legendary}</h2>
            <h2> Is Mythical: {mythical}</h2>




        </div>
    )
}

export default Pokemon
